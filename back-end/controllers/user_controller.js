var requestJSON = require('request-json');
require('dotenv').config();
const apikeyMLab = "apiKey=" + process.env.MLAB_API_KEY;
const baseMLabURL = process.env.MLAB_BASE_URL;

//exportando funciones
module.exports.getUsers = getUsers;
module.exports.getUserDetails = getUserDetails;
module.exports.getAccounts = getAccounts;
module.exports.createUsers = createUsers;
module.exports.updateUsers = updateUsers;
module.exports.updateUsersMlab = updateUsersMlab;
module.exports.deleteUsers = deleteUsers;
module.exports.loginUsers = loginUsers;
module.exports.logoutUsers = logoutUsers;

function getUsers(request, response){
  var httpClient = requestJSON.createClient(baseMLabURL);
  var queryString = 'f={"_id":0}&';
  httpClient.get('user?' + queryString + apikeyMLab,
    function(err, respuestaMLab, body){
      var respuesta = [];
      if(err){
        respuesta = {"msg" : "Error obteniendo Usuario."};
        response.status(500);
      }else{
        if(body.length > 0){
          respuesta = body;
        }else{
          respuesta = {"msg" : "Ningun elemento 'ser'."};
          response.status(404);
        }
      }
      response.send(respuesta);
    });
}

function getUserDetails(request, response){
  var httpClient = requestJSON.createClient(baseMLabURL);
  var queryString = 'f={"_id":0}&';
  var queryString2 = 'q={"userID":'+request.params.id+'}&';
  httpClient.get('user?' + queryString + queryString2 + apikeyMLab,
    function(err, respuestaMLab, body){
      var respuesta = [];
      if(err){
        respuesta = {"msg" : "Error obteniendo Usuario."};
        response.status(500);
      }else{
        if(body.length > 0){
          respuesta = body;
        }else{
          respuesta = {"msg" : "Ningun elemento 'ser'."};
          response.status(404);
        }
      }
      response.send(respuesta);
  });
}

function getAccounts(request, response){
  var httpClient = requestJSON.createClient(baseMLabURL);
  var queryString = 'f={"_id":0}&';
  var queryString2 = 'q={"id":'+request.params.id+'}&';
  httpClient.get('account?' + queryString + queryString2 + apikeyMLab,
    function(err, respuestaMLab, body){
      var respuesta = [];
      respuesta = !err ? body: {"msg":"usuario con este ID no encontrado"};
      response.send(respuesta);
    });
}


function createUsers(request, response){
  var httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('account?' +  apikeyMLab,
    function(err, respuestaMLab, body){
      newID = body.length + 1;
      getMaxId(function(nuevoID){
        var newUser = {
          "id" : nuevoID,
          "first_name" : request.body.first_name,
          "last_name"  : request.body.last_name,
          "email"      : request.body.email,
          "password"   : request.body.password
        };
        httpClient.post(baseMLabURL + "account?" + apikeyMLab, newUser,
         function(error, respuestaMLab, body){
           response.status(201);
           response.send(body);
         });
      });
    });
}

function getMaxId(callback){
  let maxUserId = 1;
  let httpClient = requestJSON.createClient(baseMLabURL);
  let queryObject = { "id": -1 };
  let queryStringEmail = 's=' + JSON.stringify(queryObject) + '&l=1&' + apikeyMLab;
  // get user with max ID
  httpClient.get('account?' + queryStringEmail, function(error, respuestaM , body) {
    if (!error) {
      if (body.length > 0) {
        maxUserId = body[0].id + 1;
      }
    } else {
      maxUserId = -1;
    }
    callback(maxUserId);
  });
}

function updateUsers(request, response){
  var id = request.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  var httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('account?'+ queryStringID + apikeyMLab,
    function(error, respuestaMLab, body) {
     var cambio = '{"$set":' + JSON.stringify(request.body) + '}';
     httpClient.put(baseMLabURL +'account?' + queryStringID + apikeyMLab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
       response.status(200);
       response.send(body);
      });
    });
}

function updateUsersMlab(request, response){
  var id = request.params.id;
  let userBody = request.body;
  var queryString = 'q={"id":' + id + '}&';
  var httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('account?' + queryString + apikeyMLab,
    function(err, respuestaMLab, body){
      let respuesta = body[0];
      let updatedUser = {
        "id"          : body[0].id,
        "first_name"  : request.body.first_name,
        "last_name"   : request.body.last_name,
        "email"       : request.body.email,
        "password"    : request.body.password
      };
      httpClient.put('account/' + respuesta._id.$oid + '?' + apikeyMLab, updatedUser,
        function(err, respuestaMLab, body){
          var respuesta = {};
          if(err) {
              respuesta = {
                "msg" : "Error actualizando usuario."
              }
              response.status(500);
          } else {
            if(body.length > 0) {
              respuesta = body;
            } else {
              respuesta = {
                "msg" : "Usuario actualizado correctamente."
              }
              response.status(200);
            }
          }
          response.send(respuesta);
        });
    });
}


function deleteUsers(request, response){
  var id = request.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  var httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('account?' +  queryStringID + apikeyMLab,
    function(error, respuestaMLab, body){
      var respuesta = body[0];
      httpClient.delete(baseMLabURL + "account/" + respuesta._id.$oid +'?'+ apikeyMLab,
        function(error, respuestaMLab,body){
          response.send(body);
      });
    });
}

function loginUsers(request, response){
  let email = request.body.email;
  let pass = request.body.password;
  let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
  let limFilter = 'l=1&';
  let httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('account?'+ queryString + limFilter + apikeyMLab,
    function(error, respuestaMLab, body) {
      if(!error) {
        if (body.length == 1) { // Existe un usuario que cumple 'queryString'
          let login = '{"$set":{"logged":true}}';
          httpClient.put('account?q={"id": ' + body[0].id + '}&' + apikeyMLab, JSON.parse(login),
            function(errPut, resPut, bodyPut) {
              response.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id});
              // If bodyPut.n == 1, put de mLab correcto
            });
        }
        else {
          response.status(404).send({"msg":"Usuario no válido."});
        }
      } else {
        response.status(500).send({"msg": "Error en petición a mLab."});
      }
  });
}

function logoutUsers(request, response){
  let email = request.body.email;
  let queryString = 'q={"email":"' + email + '"}&';
  let limFilter = 'l=1&';
  let httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('account?'+ queryString + limFilter + apikeyMLab,
    function(error, respuestaMLab, body) {
      if(!error) {
        if (body.length == 1) { // Existe un usuario que cumple 'queryString'
          let logout = '{"$unset":{"logged":true}}';
          httpClient.put('account?q={"id": ' + body[0].id + '}&' + apikeyMLab, JSON.parse(logout),
            function(errPut, resPut, bodyPut) {
              response.send({'msg':'Logout correcto', 'user':body[0].email, 'userid':body[0].id});
              // If bodyPut.n == 1, put de mLab correcto
            });
        }
        else {
          response.status(404).send({"msg":"Usuario no válido."});
        }
      } else {
        response.status(500).send({"msg": "Error en petición a mLab."});
      }
  });
}
