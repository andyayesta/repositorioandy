var express = require('express');
var userFile = require('./user.json');
var app = express();
const URL_BASE = '/apitechu/v1/';
const URL_BASE_2 = '/apitechu/v2/';
const PORT = process.env.PORT || 3000;
//variable para registro de nuevos usuarios
var totalUsers = 0;
//require para registro de nuevos usuarios para convertir el body de la peticion con bodyParser
var bodyParser = require('body-parser');
app.use(bodyParser.json());

//importando modulos para trabajar con varios archivos
const user_controller = require('./controllers/user_controller.js');

//----------------------- v1.0 -----------------------//
app.get(URL_BASE + 'users',
  function(request, response){
    response.send(userFile);
  }
);

//PETICION GET DE UN 'users'
app.get(URL_BASE + 'users/:id',
  function(request, response){
    let indice = request.params.id;
    let instancia = userFile[indice-1];
    let respuesta = (instancia != undefined)? instancia :{"mensajeValor":"recurso no encontrado"};
    response.status(200);
    response.send(respuesta);
  }
);

//PETICION GET para user logados
app.get(URL_BASE + 'logados',
  function(request, response){
    var listado = [];
    var i = 0;
    for (let us of userFile) {
      if(us.logged == "true"){
        listado[i] = us;
        i++;
      }
    }

    let respuesta = (listado.length > 0)? listado :{"mensaje":"No se encontraron resultados"};

    response.status(200);
    response.send(respuesta);
  }
);


//peticion GET con QUERY STRING
app.get(URL_BASE + 'usersq',
  function(request, response){
    response.send(request.query);
  }
);

//peticion GET con QUERY STRING para limitar el umero de usuarios a mostrar
app.get(URL_BASE + 'limitados',
  function(request, response){
    var listado = [];
    var i = 0;
    var x = 0;
    //valida numericos
     if(!isNaN(request.query.ini)  && !isNaN(request.query.long) ){
       for (let us of userFile) {
          if(x + 1 >= request.query.ini){
            if(i < request.query.long){
              listado[i] = us;
              i++;
            }
          }
         x++;
       }
       response.send({
         "mensaje" : "Respuesta Correcta",
         "listado" : listado
       });
     }else{
       response.send({
         "mensaje" : "Datos de Entrada incorrectos!"
       });
     }
  }
);

//peticion POST
app.post(URL_BASE + 'users',
    function(req, res){
        totalUsers = userFile.length;
        var longitud = Object.keys(req.body).length;
        if(longitud > 0){
          let newUser = {
            idUsuario   : totalUsers + 1,
            first_name  : req.body.first_name,
            last_name   : req.body.last_name,
            email       : req.body.email,
            password    : req.body.password
          }
          userFile.push(newUser);
          res.status(201);
          res.send({
            "mensaje" : "Usuario creado con èxito", "Usuario" : newUser
          });
      }else{
          res.send({
            "mensaje" : "Informaciòn envaida incorrectamente"
          });
      }
    }
);

//peticion PUT , terminar
app.put(URL_BASE + 'users/:id',
    function(req, res){
        totalUsers = userFile.length;
        var longitud = Object.keys(req.body).length;
        if(longitud > 0){
          let idx = req.params.id - 1;
          if(userFile[idx] != undefined){
            userFile[idx].first_name = req.body.first_name;
            userFile[idx].last_name = req.body.last_name;
            userFile[idx].email = req.body.email;
            userFile[idx].password = req.body.password;
            res.status(200);
            res.send({
              "mensaje" : "Usuario modificado con èxito", "Usuario" : userFile[idx]
            });
          }
      }else{
          res.send({
            "mensaje" : "Informaciòn envaida incorrectamente"
          });
      }
    }
);


//PETICION detele DE UN 'users', parametros splice (a,b) donde a indica la posicion y b cuantos registros
//borrar a partir de ahi
app.delete(URL_BASE + 'users/:id',
  function(request, response){
    let indice = request.params.id - 1;
    if(userFile[indice] != undefined){
        userFile.splice(request.params.id,1);
        response.status(200);
        response.send({
          "mensaje" : "Usuario eliminado con éxito", "Usuario" : JSON.stringify(userFile[indice])
        });
    }else{
        response.send({
          "mensaje" : "ERROR FATAL"
        });
    }

  }
);

//peticion POST para el LOGIN. se Hace POST porque en realidad vamos a crear
//un nuevo atributo
app.post(URL_BASE + 'login',
    function(req, res){
      var email       = req.body.email;
      var password    = req.body.password;

      if(email.length > 0 && password.length > 0){
          var indOK = false;
          for (let us of userFile) {
            if(us.email == email && us.password == password){
                  if(us.logged == "true"){
                    res.send({
                      "codigo"  : "AV002",
                      "mensaje" : "Ya ha iniciado sesion"
                    });
                  }else{
                    us.logged = "true";
                    indOK = true;
                    writeUserDataToFile(userFile);
                    res.send({
                      "codigo"  : "AV001",
                      "mensaje" : "Login correcto",
                      "isUsuario" : us.idUsuario
                    });
                  }
            }
          }
          if(!indOK){
            res.send({
              "codigo"  : "ERR002",
              "mensaje" : "Login incorrecto"
            });
          }
      }else{
        res.send({
          "codigo"  : "ERR001",
          "mensaje" : "Login incorrecto"
        });
      }

    }
);

//peticion POST para el LOGOUT, tambièn puede ser un put
app.post(URL_BASE + 'logout/:id',
    function(req, res){
      let indice = req.params.id;
      var indBUsqueda = false;
        for (let us of userFile) {
          if(us.idUsuario == indice){
            indBUsqueda = true;
                if(us.logged == "true"){
                  delete us.logged;
                  writeUserDataToFile(userFile);
                  res.send({
                    "codigo"  : "AV001",
                    "mensaje" : "Logout correcto",
                    "idUsuario" : us.idUsuario
                  });
                }else{
                  res.send({
                    "codigo"  : "ER001",
                    "mensaje" : "Logout incorrecto"
                  });
                }
          }
        }
        if(indBUsqueda == false){
          res.send({
            "codigo"  : "ER002",
            "mensaje" : "Logout incorrecto"
          });
        }
    }
);

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

function reqEMpty(req){

  return Object.keys(req.body).length !== 0 ? true : false;

}

app.listen(PORT,
  function(){
    console.log('Listener TECH-U..');
  }
);

//----------------------- v2.0 -----------------------//

//---peticion GET con MONGODB
app.get(URL_BASE_2 + 'users',user_controller.getUsers);

//---peticion GET con ID con MONGODB
app.get(URL_BASE_2 + 'users/:id',user_controller.getUserDetails);

//peticion GET con ID
app.get(URL_BASE_2 + 'users/:id/accounts', user_controller.getAccounts);

//peticion POST -> crear usuario en account
app.post(URL_BASE_2 + 'users', user_controller.createUsers);

//Peticiòn PUT -> modificar usuario en account
//Respuesta N -> 1 : Usuario modificado correctamente
app.put(URL_BASE_2 + 'users/:id', user_controller.updateUsers);

// Petición PUT con id de mLab
 app.put(URL_BASE_2 + 'usersmLab/:id',user_controller.updateUsersMlab);

//Peticion DELETE User en accounts
 app.delete(URL_BASE_2 + "users/:id",user_controller.deleteUsers);

//Peticion POST - Login
app.post(URL_BASE_2 + "login",user_controller.loginUsers);

//Peticion POST - Logout
app.post(URL_BASE_2 + "logout",user_controller.logoutUsers);
